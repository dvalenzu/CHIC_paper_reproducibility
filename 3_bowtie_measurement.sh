#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

BT_BUILD="./CHIC//ext/BOWTIE2/bowtie2-2.3.0/bowtie2-build"
BT_ALIGN="./CHIC//ext/BOWTIE2/bowtie2-2.3.0/bowtie2"
READS_FILE="./data/NA12878.1M.fastq"

echo ""
echo "====================="
echo "====================="
echo "REFS = 1"
echo "====================="
echo "====================="
echo ""
echo "---------------------------"
echo "INDEXING:"
echo "---------------------------"
echo ""
STARTTIME=$(date +%s)
${BT_BUILD} --threads=8 --quiet ./data/chr21_vars/PG_1/chr21.fa ./data/chr21_vars/PG_1/chr21.fa_BT2
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to compute..."
echo "-----"
echo ""
echo "---------------------------"
echo "ALIGNING & EVAL"
echo "---------------------------"
echo ""
STARTTIME=$(date +%s)
${BT_ALIGN} --quiet --score-min C,10,0 -x ./data/chr21_vars/PG_1/chr21.fa_BT2  -U ${READS_FILE} -S bt_out1.sam
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to compute..."
./eval_sam_tools/eval_a_sam.sh ${READS_FILE}  bt_out1.sam

echo ""
echo "====================="
echo "====================="
echo "REFS = 201"
echo "====================="
echo "====================="
echo ""
echo "INDEXING:"
echo "---------------------------"
echo ""
STARTTIME=$(date +%s)
${BT_BUILD} --threads=8 --quiet ./data/chr21_vars/PG_100/chr21_201versions.fa   ./data/chr21_vars/PG_100/chr21_201versions.fa_BT2
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to compute..."
echo "-----"
echo ""
echo "---------------------------"
echo "ALIGNING & EVAL"
echo "---------------------------"
echo ""
STARTTIME=$(date +%s)
${BT_ALIGN} --quiet --score-min C,10,0 -x ./data/chr21_vars/PG_100/chr21_201versions.fa_BT2  -U ${READS_FILE} -S bt_out201.sam
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to compute..."
./eval_sam_tools/eval_a_sam.sh ${READS_FILE}  bt_out201.sam
echo "-----"

