#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

BIO_UTILS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source "${BIO_UTILS_DIR}/utils.sh"
SAMTOOLS_BIN="${BIO_UTILS_DIR}/ext/samtools-0.1.19/samtools"

function bio_count_unique_reads_sam 
{
  local FILE_NAME=${1}
  ANS=`cat ${FILE_NAME} | grep -Ev "^@" | sed "s/\t.*//" | sort | uniq | wc -l`
  echo ${ANS}
}

function bio_mix_reads
{
  if [ "$#" -ne 2 ]; then
    echo "Function name:  ${FUNCNAME}"
    echo "uses 3 params instead of $#" 
    utils_die 
  fi
	local R1=$1
	local R2=$2
	utils_assert_file_exists ${R1}
	utils_assert_file_exists ${R2}
  
  R_DIR=$(dirname "${R1}")
  local OUTPUT="${R_DIR}/reads_ALL_RENAMED.fq.gz"
  if [[ -f "${OUTPUT}" ]]; then
    echo >&2 -e "${OUTPUT} exists, will reuse it"
    return
  fi
	
	if [ ${R1: -3} == ".gz" ]; then
    awk '{if ((NR-1)%4==0) print "@"1+(NR-1)/4; else print }' <(zcat ${R1}) <(zcat ${R2}) | gzip > ${OUTPUT}
	else
		awk '{if ((NR-1)%4==0) print "@"1+(NR-1)/4; else print }' ${R1} ${R2} | gzip > ${OUTPUT}
	fi
}

function bio_count_unique_reads_fq 
{
  local FILE_NAME=${1}
	local MYCAT="cat"
	if [ ${FILE_NAME: -3} == ".gz" ]; then
		MYCAT="zcat"
	fi
	

  #ANS=`cat ${FILE_NAME} | awk 'NR % 4 == 1' | sort | uniq | wc -l`
  ANS=`${MYCAT} ${FILE_NAME} | awk 'NR % 4 == 1' | tr ' ' '_' | cut -c 2- | sort | uniq | wc -l`
  echo ${ANS}
}

## TODO: remove the other one and merge
function bio_validate_outputSAM_inputFQ
{
	if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
  	echo "uses 2 or 3 params, but instead it recieved  $#" 
  	exit 33
	fi
  
	local SAM_NAME=${1}
  local READS_1=${2}
  local READS_2=""
	if [ "$#" -eq 3 ]; then
  	READS_2=${3}
  	utils_assert_file_exists ${READS_2}
	fi
  utils_assert_file_exists ${SAM_NAME}
  utils_assert_file_exists ${READS_1}
  N_SAM=$( bio_count_unique_reads_sam ${SAM_NAME} )
  N_FQ_1=$( bio_count_unique_reads_fq ${READS_1} )
 	
	N_FQ_2="0"
	if [ "$#" -eq 3 ]; then
		N_FQ_2=$( bio_count_unique_reads_fq ${READS_2} )
	fi
	
	local N_READS=$((${N_FQ_1} + ${N_FQ_2})) 

  return 0
  if [[ ${N_SAM} -eq ${N_READS} ]]; then
    return 0
  else
    ## TODO: this should go to std err.
    echo "********************"
    echo "********************"
    echo "********************"
    echo "Different number of reads, will abort, and offer you to check."
		echo "${SAM_NAME}"
    echo "********************"
    FLUSH_SAM=`cat ${SAM_NAME} | grep -Ev "^@" | sed "s/\t.*//" | sort | uniq > dbg_sam.txt`
		
		MYCAT="cat"
		if [ ${READS_1: -3} == ".gz" ]; then
			MYCAT="zcat"
		fi
    FLUSH_READS_1=`${MYCAT} ${READS_1} | awk 'NR % 4 == 1' | tr ' ' '_' | cut -c 2- | sort | uniq > dbg_reads1.txt`
		#TODO: if nargs imply two reads:
    FLUSH_READS_2=`${MYCAT} ${READS_2} | awk 'NR % 4 == 1' | tr ' ' '_' | cut -c 2- | sort | uniq > dbg_reads2.txt`
    utils_die "Lost read :(. You can vimdiff dbg_*.txt to see the lost read"
  fi
}

function eval_mapped_unmapped_SAM 
{
  if [ "$#" -ne 1 ]; then
    echo "Function name:  ${FUNCNAME}"
    echo "uses 1 params instead of $#" 
    utils_die 
  fi
  SAM_FILE=`readlink -f ${1}`
  utils_assert_file_exists ${SAM_FILE}
  #echo "Processing ${SAM_FILE}:"
	utils_assert_file_exists ${SAMTOOLS_BIN}


  N_UNMAPPED=`$SAMTOOLS_BIN view -S -c -f 4 ${SAM_FILE} 2>> samtools_errors.log`
  #N_MAPPED=`${SAMTOOLS_BIN} view -S -c -F 0x904 ${SAM_FILE} 2>> samtools_errors.log`
  N_MAPPED=`${SAMTOOLS_BIN} view -S -F 4 ${SAM_FILE}  2>> samtools_errors.log  | cut -f1 | sort | uniq | wc -l `
  N_TOT=$((${N_MAPPED} + ${N_UNMAPPED})) 
  echo "*********************" 
  echo "Counts for  ${SAM_FILE}:"
  echo "${N_UNMAPPED} reads un-mapped"
  echo "${N_MAPPED} reads mapped"
  echo "Total: ${N_TOT}"
  echo "*********************" 
}

function report_chic_size 
{
  if [ "$#" -ne 1 ]; then
    echo "Function name:  ${FUNCNAME}"
    echo "uses 1 params instead of $#" 
    utils_die 
  fi
  FASTA_FILE=`readlink -f ${1}`

  utils_assert_file_exists ${FASTA_FILE}
  echo "Processing ${FASTA_FILE}:"
  #SUFFIX_LIST=( ".book_keeping" ".is_literal" ".limits" ".limits_kernel" ".P512_GC4_kernel_text.amb" ".P512_GC4_kernel_text.ann" ".P512_GC4_kernel_text.bwt" ".P512_GC4_kernel_text.pac" ".P512_GC4_kernel_text.sa" ".ptr" ".rmq" ".sparse_sample_limits_kernel" ".sparseX" ".variables" ".x" )
  
  #FILE_LIST=""
  #for SUFFIX in ${SUFFIX_LIST[@]}
  #do
  #  TARGET_FILE="${FASTA_FILE}${SUFFIX}"
  #  utils_assert_file_exists ${TARGET_FILE}
  #  FILE_LIST="${FILE_LIST} ${TARGET_FILE}"
  #done
  FILE_LIST="${FASTA_FILE}.*"
  ls ${FILE_LIST} 
  du ${FILE_LIST} -hc
  #SIZE=`du -hcs ${FILE_LIST} | cut -f1`
  #echo "Size is ${SIZE}"
}

function report_bwa_size 
{
  if [ "$#" -ne 1 ]; then
    echo "Function name:  ${FUNCNAME}"
    echo "uses 1 params instead of $#" 
    utils_die 
  fi
  FASTA_FILE=`readlink -f ${1}`
  TMP_FOLDER="./tmp_chic_files"
  rm -rf ${TMP_FOLDER}
  mkdir ${TMP_FOLDER};

  utils_assert_file_exists ${FASTA_FILE}
  echo "Processing ${FASTA_FILE}:"
  SUFFIX_LIST=( ".amb" ".ann" ".bwt" ".pac" ".sa" )
  for SUFFIX in ${SUFFIX_LIST[@]}
  do
    TARGET_FILE="${FASTA_FILE}${SUFFIX}"
    #echo "Suffix: ${SUFFIX}"
    #echo "Suffix: ${TARGET_FILE}"
    utils_assert_file_exists ${TARGET_FILE}
    cp ${TARGET_FILE} ${TMP_FOLDER}
  done
  SIZE=`du -hcs ${TMP_FOLDER} | cut -f1`
  echo "Size is ${SIZE}"
}

function eval_alignment_bam
{
	if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
		echo "Function name:  ${FUNCNAME}"
		echo "uses 2 or 3 params instead of $#" 
		utils_die 
	fi

	TMP_SAM_FILE=./tmp_eval.sam
	if [ "$#" -eq 2 ]; then
		READS_FQ_FILE=${1}
		BAM_FILE=${2}
		${SAMTOOLS_BIN} view -h ${BAM_FILE} > ${TMP_SAM_FILE}
		eval_alignment_sam ${READS_FQ_FILE} ${TMP_SAM_FILE}
	elif [ "$#" -eq 3 ]; then
		READS_1=${1}
		READS_2=${2}
		BAM_FILE=${3}
		${SAMTOOLS_BIN} view -h ${BAM_FILE} > ${TMP_SAM_FILE}
		eval_alignment_sam ${READS_1} ${READS_2} ${TMP_SAM_FILE}
	else
		echo "Impossible happend."
	fi
	rm ${TMP_SAM_FILE}
}

function eval_alignment_sam
{
	if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
		echo "Function name:  ${FUNCNAME}"
		echo "uses 2 or 3 params instead of $#" 
		utils_die 
	fi

	if [ "$#" -eq 2 ]; then
		READS_FQ_FILE=${1}
		SAM_FILE=${2}
		utils_assert_file_exists ${READS_FQ_FILE}
		utils_assert_file_exists ${SAM_FILE}
		bio_validate_outputSAM_inputFQ ${SAM_FILE} ${READS_FQ_FILE}
	elif [ "$#" -eq 3 ]; then
		READS_1=${1}
		READS_2=${2}
		SAM_FILE=${3}
		utils_assert_file_exists ${READS_1}
		utils_assert_file_exists ${READS_2}
		utils_assert_file_exists ${SAM_FILE}
		bio_validate_outputSAM_inputFQ ${SAM_FILE} ${READS_1} ${READS_2}
	else
		echo "Impossible happend."
	fi


	echo "**********"
  echo "SAM REPORT"
  echo "**********"
  eval_mapped_unmapped_SAM ${SAM_FILE} 
  #echo "SIZE REPORT"
  #echo "**********"
  #report_chic_size ${FASTA_FILE}
}


function bio_sort_and_compare_sam 
{
	if [ "$#" -ne 2 ] ; then
		echo "Function name:  ${FUNCNAME}"
		echo "uses 2 params instead of $#" 
		utils_die 
	fi
	SAM_1=${1}
	SAM_2=${2}
	sort ${SAM_1} > tmp_sorted_1.sam
	sort ${SAM_2} > tmp_sorted_2.sam
	utils_assert_files_are_equal tmp_sorted_1.sam tmp_sorted_2.sam
  rm tmp_sorted_1.sam tmp_sorted_2.sam
}


