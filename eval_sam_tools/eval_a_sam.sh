#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "${DIR}/utils.sh"
source "${DIR}/bio_utils.sh"

if [ "$#" -ne 2 ]; then
  echo "Script: '${0}'"
  echo "uses 2 params, but instead it recieved  $#" 
  exit 33
fi

utils_assert_file_exists ${1}
utils_assert_file_exists ${2}

READS_FILE=`readlink -f ${1}`
SAM_FILE=`readlink -f ${2}`


echo "=================================="
echo "Evaluation of Alignment..."
echo "----------------------------------"
eval_alignment_sam ${READS_FILE} ${SAM_FILE}
echo "=================================="
echo ""

