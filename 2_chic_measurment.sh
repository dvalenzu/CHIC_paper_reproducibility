#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

READS_FILE="./data/NA12878.1M.fastq"

### EXPERIMENTS #####
#######################

echo ""
echo "====================="
echo "====================="
echo "REFS = 1001"
echo "====================="
echo "====================="
echo ""
echo "---------------------------"
echo "INDEXING:"
echo "---------------------------"
echo ""
./CHIC/src/chic_index ./data/chr21_vars/PG_500/chr21_1001versions.fa 100 --threads=8 --verbose=2 --kernel=BOWTIE2 --max-edit-distance=10  --lz-parsing-method=RLZ --rlz-ref-size=50  --mem=8000

echo ""
echo "---------------------------"
echo "ALIGNING & EVAL"
echo "---------------------------"
echo ""
./CHIC/src/chic_align --secondary=NONE --threads=8 --kernel-options=--n-ceil=C,10,0  ./data/chr21_vars/PG_500/chr21_1001versions.fa ${READS_FILE}   --output=out1001.sam
./eval_sam_tools/eval_a_sam.sh ${READS_FILE}  out1001.sam

echo ""
echo "====================="
echo "====================="
echo "REFS = 201"
echo "====================="
echo "====================="
echo ""
echo "---------------------------"
echo "INDEXING:"
echo "---------------------------"
echo ""
./CHIC/src/chic_index ./data/chr21_vars/PG_100/chr21_201versions.fa 100 --threads=8 --verbose=2 --kernel=BOWTIE2 --max-edit-distance=10  --lz-parsing-method=RLZ --rlz-ref-size=50  --mem=8000
echo ""
echo "---------------------------"
echo "ALIGNING & EVAL"
echo "---------------------------"
echo ""
./CHIC/src/chic_align --secondary=NONE --threads=8 --kernel-options=--n-ceil=C,10,0  ./data/chr21_vars/PG_100/chr21_201versions.fa ${READS_FILE}   --output=out201.sam
./eval_sam_tools/eval_a_sam.sh ${READS_FILE}  out201.sam

echo ""
echo "====================="
echo "====================="
echo "REFS = 1"
echo "====================="
echo "====================="
echo ""
echo "INDEXING:"
echo "---------------------------"
echo ""
./CHIC/src/chic_index ./data/chr21_vars/PG_1/chr21.fa 100 --threads=8 --verbose=2 --kernel=BOWTIE2 --max-edit-distance=10  --lz-parsing-method=RLZ --rlz-ref-size=50  --mem=8000
echo "ALIGNING & EVAL"
echo "---------------------------"
echo ""
./CHIC/src/chic_align --secondary=NONE --threads=8 --kernel-options=--n-ceil=C,10,0  ./data/chr21_vars/PG_1/chr21.fa ${READS_FILE}   --output=out1.sam
./eval_sam_tools/eval_a_sam.sh ${READS_FILE}  out1.sam
