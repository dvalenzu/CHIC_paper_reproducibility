#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

LIST="./data/chr21_vars/PG_500/chr21_1001versions.fa ./data/chr21_vars/PG_100/chr21_201versions.fa ./data/chr21_vars/PG_1/chr21.fa"
for FILE in ${LIST} 
do
	rm -f ${FILE}.*
	rm -f ${FILE}_*
done


