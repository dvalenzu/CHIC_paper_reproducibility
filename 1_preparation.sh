#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

### DOWNLOAD CHIC #####
#######################

git clone git@gitlab.com:dvalenzu/CHIC.git
cd CHIC
git checkout develop
make 
cd ..

### DOWNLOAD DATA #####
#######################
mkdir -p data
cd data
wget https://www.cs.helsinki.fi/u/dvalenzu/data/chic/NA12878.1M.fastq.xz	
wget https://www.cs.helsinki.fi/u/dvalenzu/data/chic/chr21_vars.tar.xz

unxz NA12878.1M.fastq.xz	
tar -xvf chr21_vars.tar.xz
cd ..

